﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    static private CameraShake instance;
    static public void StartShake(Callback onStart, Callback onEnd) {
        instance.Play(onStart, onEnd);
    }

    static public void StopShake() {
        instance.Stop();
    }

    private Coroutine shake = null;

    public delegate void Callback();

    public float duration = 1f;
    public float magnitude = 3f;


    void Awake() {
        instance = this;
    }

    private IEnumerator Shake(Callback onStart, Callback onEnd) {
        CameraFollow.StopFollow();

        Vector3 originalPos = transform.position;
        float elapsed = 0;

        onStart?.Invoke();

        while (elapsed < duration) {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.position = new Vector3(originalPos.x + x, originalPos.y + y, originalPos.z);
            elapsed += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        onEnd?.Invoke();

        transform.position = originalPos;

        shake = null;
        CameraFollow.StartFollow();
    }

    private void Play(Callback onStart, Callback onEnd) {
        if (shake == null)
            shake = StartCoroutine(Shake(onStart, onEnd));
    }
    private void Stop() {
        StopCoroutine(shake);
        shake = null;
    }
}
