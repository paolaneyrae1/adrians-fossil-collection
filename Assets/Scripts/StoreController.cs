﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StoreController : MonoBehaviour {

    [System.Serializable]
    public struct Pack {
        public int price;
        public int quantity;
    }

    public Pack[] packs;
    public Transform content;
    public TextMeshProUGUI txtScore;

    void Start() {
        SetActive(false);
    }

    void Update() {
        txtScore.text = GameManager.currentScore.ToString();
    }

    private void UpdatePacks() {

        for (int i = 0; i < packs.Length; i++) {
            Transform pack = content.GetChild(i);

            Image image = pack.GetComponentInChildren<Image>();

            Button btn = pack.GetComponentInChildren<Button>();
            if (btn != null)
                btn.interactable = GameManager.currentScore >= packs[i].price;
        }

    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);
        UpdatePacks();

        if (value)
            GameManager.PlayBGOther();
        else
            GameManager.PlayBGMain();
    }

    public void BuyPack(int id) {

        if (id == 3)
        {
            GameManager.AddTime(25, Vector3.zero);
            GameManager.ScoreDecrease(packs[id].price);
            UpdatePacks();
        }

        else
        {
            GameManager.ScoreDecrease(packs[id].price);

            ItemData[] items = ItemsDB.GetItems(packs[id].quantity);
            for (int i = 0; i < items.Length; i++)
                GameManager.AddPurchasedItem(items[i]);

            UpdatePacks();
        }
    }
}
