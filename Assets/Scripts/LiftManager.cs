﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftManager : MonoBehaviour {

    static private LiftManager instance;

    static public void Load(Lift lift) {
        if (instance.state == State.empty) {
            instance.state = State.loaded;

            for (int i = 0; i < instance.lifts.Length; i++)
                if (instance.lifts[i] == lift)
                    instance.currentLiftIndex = i;
        }
    }

    static public void Unload() {
        if (instance.state == State.loaded) {
            instance.state = State.empty;
            instance.currentLiftIndex = -1;
        }
    }

    static public void Up() {
        instance.UpLevel();
    }

    static public void Down() {
        instance.DownLevel();
    }

    static public void Pause() {
        instance.speed = 0;

        foreach (Lift lift in instance.lifts)
            lift.PauseFx();
    }

    static public void Continue() {
        instance.speed = 1f;

        foreach (Lift lift in instance.lifts)
            lift.UnPauseFx();
    }
    private enum State {
        empty, loaded, moving
    }
    [SerializeField]
    private State state = State.empty;

    [SerializeField]
    private int currentLevel = 0;
    [SerializeField]
    private int currentLiftIndex = -1;

    public float speed = 1f;
    public Transform[] levels;

    public float minDistance = 1.4f;

    public Lift[] lifts;

    // Start is called before the first frame update
    void Start() {
        instance = this;
    }

    // Update is called once per frame
    void Update() {

    }
    private IEnumerator Move(int from, int to) {
        float t = 0;
        state = State.moving;

        foreach (Lift lift in lifts)
            lift.PlayFx();


        Player.position = new Vector3(
            lifts[currentLiftIndex].transform.position.x, 
            lifts[currentLiftIndex].transform.position.y + 0.655f);

        foreach (Lift lift in lifts)
            lift.gateController.Close();

        yield return new WaitForSeconds(.3f);

        while (t < 1f) {
            float posY = Mathf.Lerp(levels[from].position.y, levels[to].position.y, t);
            t += speed * Time.deltaTime;


            foreach (Lift lift in lifts)
                lift.transform.position = new Vector3(lift.transform.position.x, posY);


            yield return new WaitForEndOfFrame();
        }

        foreach (Lift lift in lifts)
            lift.transform.position = new Vector3(lift.transform.position.x, levels[to].position.y);


        foreach (Lift lift in lifts)
            lift.gateController.Open();

        if (Player.currentState != Player.State.waiting)
            Player.Unlock();

        foreach (Lift lift in lifts)
            lift.StopFx();

        currentLevel = to;
        state = State.loaded;
    }



    public void UpLevel() {
        if (currentLevel <= 0 || state != State.loaded)
            return;

        StartCoroutine(Move(currentLevel, currentLevel - 1));
    }

    public void DownLevel() {
        if (currentLevel >= levels.Length - 1 || state != State.loaded)
            return;

        StartCoroutine(Move(currentLevel, currentLevel + 1));
    }
}
