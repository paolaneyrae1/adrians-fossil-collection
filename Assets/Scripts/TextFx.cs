﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextFx : MonoBehaviour {

    public float delay = 1f;
    public TextMeshProUGUI txtTime;


    public int value {
        set {
            txtTime.text = "+" + value.ToString();
        }
    }

    void Start() {
        Destroy(gameObject, delay);
    }

}
