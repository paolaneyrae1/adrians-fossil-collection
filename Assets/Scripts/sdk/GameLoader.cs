﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LoLSDK;
using SimpleJSON;
using System.IO;

public class GameLoader : MonoBehaviour {

    static private GameLoader instance;

    static public void SetCurrentLevelIndex(int index) {
        instance.playerData.last_level_index = index;
    }

    static public int GetCurrentLevelIndex() {
        return instance.playerData.last_level_index;
    }

    static public void FinishCurrentLevel() {
        if (instance.playerData.last_level_index > instance.playerData.max_level_index)
            instance.playerData.max_level_index = Mathf.Min(instance.playerData.last_level_index, 16);

        instance.playerData.last_level_index++;
    }

    static public void SetCoins(int coins) {
        instance.playerData.coins = coins;
    }

    static public void SetFossils(List<Fossil> fossils) {
        instance.playerData.fossils = fossils;
    }

    static public void SetMuseum(Museum museum) {
        instance.playerData.museum = museum;
    }

    static public void Save() {
        LOLSDK.Instance.SaveState(instance.playerData);
    }

    static public void Load(System.Action<PlayerData> callback) {
        LOLSDK.Instance.LoadState<PlayerData>(state => {
            if (state != null) {
                instance.playerData = state.data;
                LOLSDK.Instance.SubmitProgress(state.score, instance.playerData.max_level_index, 13);
            }
            callback?.Invoke(instance.playerData);
        });
    }

    static public void Complete() {
        LOLSDK.Instance.CompleteGame();
    }

    // Relative to Assets /StreamingAssets/
    private const string languageJSONFilePath = "language.json";
    private const string startGameJSONFilePath = "startGame.json";

    [SerializeField]
    [Header("Initial State Data")]
    private PlayerData playerData;


    public Button newGameButton;
    public Button continueButton;

    void Awake() {
        if (instance != null)
            Destroy(instance.gameObject);

        instance = this;
        DontDestroyOnLoad(gameObject);


        // Create the WebGL (or mock) object
#if UNITY_EDITOR
        ILOLSDK webGL = new LoLSDK.MockWebGL();
#elif UNITY_WEBGL
			ILOLSDK webGL = new LoLSDK.WebGL();
#endif


        if (!LOLSDK.Instance.IsInitialized) {
            // Initialize the object, passing in the WebGL
            LOLSDK.Init(webGL, "com.legends-of-learning.unity.sdk.v5.2.adrians-fossil-collection");

            // Register event handlers
            LOLSDK.Instance.StartGameReceived += new StartGameReceivedHandler(this.HandleStartGame);
            LOLSDK.Instance.GameStateChanged += new GameStateChangedHandler(this.HandleGameStateChange);
            LOLSDK.Instance.LanguageDefsReceived += new LanguageDefsReceivedHandler(this.HandleLanguageDefs);

            // Mock the platform-to-game messages when in the Unity editor.
#if UNITY_EDITOR
            LoadMockData();
#endif

            // Used for player feedback. Not required by SDK.
            LOLSDK.Instance.SaveResultReceived += OnSaveResult;


            // Then, tell the platform the game is ready.
            LOLSDK.Instance.GameIsReady();


            Application.runInBackground = false;
        }


        // Helper method to hide and show the state buttons as needed.
        // Will call LoadState<T> for you.
        Helper.StateButtonInitialize<PlayerData>(newGameButton, continueButton, OnLoad);
    }

    private void OnDestroy() {
#if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            return;
#endif
        LOLSDK.Instance.SaveResultReceived -= OnSaveResult;
    }


    // Start the game here
    void HandleStartGame(string json) {
        SharedState.StartGameData = JSON.Parse(json);
        //SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    // Handle pause / resume
    void HandleGameStateChange(GameState gameState) {
        // Either GameState.Paused or GameState.Resumed
        Debug.Log("HandleGameStateChange");
    }

    // Use language to populate UI
    void HandleLanguageDefs(string json) {
        JSONNode langDefs = JSON.Parse(json);

        // Example of accessing language strings
        //Debug.Log(langDefs);
        //Debug.Log(langDefs["welcome"]);

        SharedState.LanguageDefs = langDefs;
    }


    /// <summary>
    /// This is the setting of your initial state when the scene loads.
    /// The state can be set from your default editor settings or from the
    /// users saved data after a valid save is called.
    /// </summary>
    /// <param name="loadedPlayerData"></param>
    void OnLoad(PlayerData loadedPlayerData) {
        // Overrides serialized state data or continues with editor serialized values.
        if (loadedPlayerData != null) {
            playerData = loadedPlayerData;

            SceneManager.LoadScene(playerData.last_level_index);
        }
        else {
            LOLSDK.Instance.SaveState(playerData);
        }
    }

    void OnSaveResult(bool success) {
        if (!success) {
            Debug.LogWarning("Saving not successful");
            return;
        }


    }

    private void LoadMockData() {
#if UNITY_EDITOR
        // Load Dev Language File from StreamingAssets

        string startDataFilePath = Path.Combine(Application.streamingAssetsPath, startGameJSONFilePath);
        string langCode = "en";


        if (File.Exists(startDataFilePath)) {
            string startDataAsJSON = File.ReadAllText(startDataFilePath);
            JSONNode startGamePayload = JSON.Parse(startDataAsJSON);
            // Capture the language code from the start payload. Use this to switch fontss
            langCode = startGamePayload["languageCode"];
            HandleStartGame(startDataAsJSON);
        }

        // Load Dev Language File from StreamingAssets
        string langFilePath = Path.Combine(Application.streamingAssetsPath, languageJSONFilePath);
        if (File.Exists(langFilePath)) {
            string langDataAsJson = File.ReadAllText(langFilePath);
            // The dev payload in language.json includes all languages.
            // Parse this file as JSON, encode, and stringify to mock
            // the platform payload, which includes only a single language.
            JSONNode langDefs = JSON.Parse(langDataAsJson);
            // use the languageCode from startGame.json captured above
            HandleLanguageDefs(langDefs[langCode].ToString());
        }


#endif
    }
}
