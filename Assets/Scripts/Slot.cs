﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Slot : MonoBehaviour, IPointerClickHandler {

    private ItemData _item;
    public ItemData item {
        get => _item;
        set => _item = value;
    }

    private TextMeshProUGUI txtStack {
        get {
            return transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        }
    }

    public GameObject cursor;
    public Sprite sprite {
        set {
            GetComponent<Image>().sprite = value;
        }
    }

    private int _stack;
    public int stack {
        get { return _stack; }
        set {
            _stack = value;

            txtStack.text = "x" + _stack.ToString("00");
        }
    }

    // Start is called before the first frame update
    void Start() {
        cursor.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.clickCount == 1)
            Inventory.FossilSelect(transform.GetSiblingIndex());

        if (eventData.clickCount == 2)
            Inventory.FossilDetail(item);
    }

    public void SetParent(Transform p) {
        transform.SetParent(p);
    }

    public void Selected(bool value) {
        cursor.SetActive(value);
    }
    
}
