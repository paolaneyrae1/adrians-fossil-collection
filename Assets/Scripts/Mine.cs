﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour {

    public Lift lift;

    public Transform smallWheel;
    public Transform bigWheel;

    public float speed = 100f;


    void Update() {
        if (!lift.isMoving)
            return;

        smallWheel.Rotate(Vector3.forward, speed * Time.deltaTime);
        bigWheel.Rotate(Vector3.forward, -speed * Time.deltaTime);
    }
}
