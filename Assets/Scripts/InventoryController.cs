﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour {

    public Inventory inventory;

    public bool isOpen {
        get { return gameObject.activeSelf; }
    }

    // Start is called before the first frame update
    void Start() {
        SetActive(false);
    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            GameManager.PlayBGOther();
        else
            GameManager.PlayBGMain();
    }

    public void AddItem(ItemData item) {
        inventory.AddItem(item);
    }
}
