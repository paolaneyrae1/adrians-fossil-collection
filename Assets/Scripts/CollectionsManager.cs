﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionsManager : MonoBehaviour {

    static private CollectionsManager instance;


    static public void SetActiveTrexPart(int index, bool value) {
        instance.collections[0].SetActivePart(index, value);

    }
    static public void EnableMedal(int index) {
        instance.medal[index].SetActive(true);
    }
    static public void SetActiveTriceratopsPart(int index, bool value) {
        instance.collections[1].SetActivePart(index, value);

    }

    static public void SetActiveVelociraptorPart(int index, bool value) {
        instance.collections[2].SetActivePart(index, value);

    }

    static public Museum ExportMuseum() {
        Museum museum = new Museum();

        museum.trex = instance.collections[0].Export();
        museum.triceratops = instance.collections[1].Export();
        museum.velociraptor = instance.collections[2].Export();

        return museum;
    }

    static public bool CheckCollectionByIndex(int index) {
        return instance.collections[index].isComplete;
    }

    static public bool CheckAnyCollection() {
        Debug.Log("Collections " + CheckCollectionByIndex(0) + " " + CheckCollectionByIndex(1) + " " + CheckCollectionByIndex(2));
        return CheckCollectionByIndex(0) || CheckCollectionByIndex(1) || CheckCollectionByIndex(2);
    }

    static public bool CheckAllCollections() {
        return CheckCollectionByIndex(0) && CheckCollectionByIndex(1) && CheckCollectionByIndex(2);
    }

    private AudioSource audioSource;

    [SerializeField]
    private float alpha;

    [Range(0f, 10f)]
    public float radio = 5f;

    [Range(0f, 360f)]
    public float offset;
    public float speed = 1;
    public AnimationCurve curve;

    public Collection[] collections;
    public GameObject[] medal;

    public enum State {
        idle, moving
    }
    public State state = State.idle;

    void Awake() {
        instance = this;
    }
   
    void Start() {
        audioSource = GetComponent<AudioSource>();
  
        CollectionsController.UpdateDescription(SharedState.LanguageDefs["collectionstrex"]);
        for (int i = 0; i < 3; i++) 
        {
            medal[i].SetActive(CheckCollectionByIndex(i));
        }
        
    }

    // Update is called once per frame
    void Update() {
        UpdateCollections();
    }

    void OnDrawGizmos() {
        UpdateCollections();

        foreach (Collection collection in collections)
            Debug.DrawLine(transform.position, collection.position);
    }


    private void UpdateCollections() {
        float interval = 360f / collections.Length;// * Mathf.Deg2Rad;
        float angle;
        Vector3 pos = Vector3.zero;

        for (int i = 0; i < collections.Length; i++) {
            angle = (interval * i) + offset + alpha;
            angle *= Mathf.Deg2Rad;

            pos.x = Mathf.Cos(angle) * radio;
            pos.y = 0;
            pos.z = Mathf.Sin(angle) * radio;
            pos += transform.position;

            collections[i].position = pos;
        }
    }

    private IEnumerator Move(float to) {
        state = State.moving;

        audioSource.Play();
        CollectionsController.HideDescription();

        float from = alpha;
        float t = 0;

        while (t < 1f) {
            alpha = Mathf.Lerp(from, to, curve.Evaluate(t));

            t += speed * Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        alpha = to;

        if (alpha >= 360f)
            alpha -= 360f;

        if (alpha <= -360f)
            alpha = 360f;

        string dino = "";
        switch (Mathf.Abs(alpha)) {
            case 0:
            case 360:
                dino = "trex";
                break;

            case 120:
                dino = "velociraptor";
                break;

            case 240:
                dino = "triceratops";
                break;
        }

        CollectionsController.ShowDescription();
        CollectionsController.UpdateDescription(SharedState.LanguageDefs["collections" + dino]);
        state = State.idle;
    }

    public void Next() {
        if (state == State.moving)
            return;

        float interval = 360f / collections.Length;
        StartCoroutine(Move(alpha + interval));
    }

    public void Previous() {
        if (state == State.moving)
            return;

        float interval = 360f / collections.Length;
        StartCoroutine(Move(alpha - interval));
    }

}
