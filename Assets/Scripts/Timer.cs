﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour {


    private enum State {
        stop, play, pause
    }

    [SerializeField]
    private TextMeshProUGUI txtTimer;
    [SerializeField]
    private float time = 300f;
    [SerializeField]
    private State state = State.stop;


    public string timeFormated {
        get {
            return txtTimer.text;
        }
    }

    public int timeInSeconds {
        get => Mathf.FloorToInt(time);
    }
    void Start() {
        state = State.stop;

        UpdateTime();
    }

    void Update() {

        if (state != State.play)
            return;

        time -= Time.deltaTime;

        if (time < 0) {
            time = 0;
            state = State.stop;

            GameManager.TimesUp();
        }

        UpdateTime();
    }

    public void UpdateTime() {
        int minutes = Mathf.FloorToInt(time) / 60;
        int seconds = Mathf.FloorToInt(time) % 60;

        txtTimer.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }

    public void Play(float time) {
        this.time = time;
        Play();
    }

    public void Play() {
        state = State.play;
    }

    public void Pause() {
        state = State.pause;
    }

    public void Stop() {
        state = State.stop;
        time = 0;
    }

    public void AddTime(float time) {
        this.time += time;
        UpdateTime();
    }
}
