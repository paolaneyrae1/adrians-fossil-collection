﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeParticle : MonoBehaviour {

    public float speed = 100f;

    void Update() {
        transform.position += Vector3.down * speed * Time.deltaTime;

        if (transform.position.y < -50f)
            Destroy(gameObject);
    }
}
