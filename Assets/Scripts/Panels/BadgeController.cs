﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BadgeController : MonoBehaviour {

    static private BadgeController instance;

    static public void Show(int index) {
        Player.Lock();
        Enemy.isLocked = true;
        GameManager.PauseTimer();

        instance.SetMask(index);
        instance.SetActive(true);

        instance.audioSource.Play();
    }

    private string dinoNameKey;

    [System.Serializable]
    public struct MaskData {
        public string dinoNameKey;
        public string statsKey;
        public GameObject mask;
    }


    public TextMeshProUGUI txtDinosaurName;
    public TextMeshProUGUI txtInfo;
    public MaskData[] maskDatas;

    public SpeechManager speechManager;
    public AudioSource audioSource;


    void Start() {
        instance = this;

        SetActive(false);
    }

    private void SetMask(int index) {
        for (int i = 0; i < maskDatas.Length; i++) {
            maskDatas[i].mask.SetActive(i == index);
        }

        dinoNameKey = maskDatas[index].dinoNameKey;
        txtDinosaurName.text = SharedState.LanguageDefs[maskDatas[index].dinoNameKey];
        txtInfo.text = SharedState.LanguageDefs[maskDatas[index].statsKey];
    }

    public void SpeakMaskName() {
        speechManager.OnClickSpeakText(dinoNameKey);
    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);
    }

    public void Close() {
        Player.Unlock();
        Enemy.isLocked = false;
        GameManager.PlayTimer();

        SetActive(false);
    }
}
