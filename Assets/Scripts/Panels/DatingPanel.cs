﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatingPanel : MonoBehaviour {

    public AudioSource aud;
    void Start() {
        SetActive(false);
        
    }
    
    public void SetActive(bool value) {
        gameObject.SetActive(value);
        
        if(value) aud.Play();
    }

    public void Show() {
        gameObject.SetActive(true);
        aud.Play();

        GameManager.PauseTimer();
        Enemy.isLocked = true;
    }

    public void Close() {
        gameObject.SetActive(false);

        GameManager.PlayTimer();
        Enemy.isLocked = false;
    }
}
