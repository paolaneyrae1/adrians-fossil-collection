﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanel : MonoBehaviour {

    private AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();

        SetActive(false);
    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            audioSource.Play();
    }

}
