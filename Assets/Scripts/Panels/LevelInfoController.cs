﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelInfoController : MonoBehaviour {

    public TextMeshProUGUI txtMision;
    public SpeechManager speechManager;
    
    public string mision {
        set => txtMision.text = value;
    }

    void Start() {
        SetActive(false);
    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            GameManager.PlayBGOther();
        else
            GameManager.PlayBGMain();
    }

    public void SpeakMission() {
        speechManager.OnClickSpeakText("missions" + Level1_1Manager.currentMissionKey);
    }

    public void CancelSpeakMission() {
        speechManager.CancelText();
    }
}
