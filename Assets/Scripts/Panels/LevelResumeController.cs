﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelResumeController : MonoBehaviour {

    public TextMeshProUGUI txtTitle;
    public TextMeshProUGUI txtTime;
    public TextMeshProUGUI txtScore;
    public TextMeshProUGUI txtFossils;
    public TextMeshProUGUI txtCollection;

    public int maxFossils;
    public int maxCollection;

    public AudioSource audioSource;

    public int score;
    public int fossils;
    public int collections;

    public string timeFormated;


    public AudioClip levelCopleted;
    public AudioClip contador;

    private IEnumerator ShowResults() {
        int currentScore = 0;
        int currentFossils = 0;
        int currentCollection = 0;

        txtScore.text = "";
        txtFossils.text = "";

        if (txtCollection != null)
            txtCollection.text = "";

        txtTime.text = timeFormated;

        audioSource.loop = true;
        audioSource.clip = contador;
        audioSource.Play();

        while (currentScore <= score) {
            txtScore.text = currentScore.ToString();
            currentScore++;

            yield return new WaitForEndOfFrame();
        }


        while (currentFossils <= fossils) {
            txtFossils.text = currentFossils.ToString() + "/" + maxFossils.ToString();
            currentFossils++;

            yield return new WaitForEndOfFrame();
        }

        audioSource.Stop();


        if (txtCollection != null)
            while (currentCollection <= collections) {
                txtCollection.text = currentCollection.ToString() + "/" + maxCollection.ToString();
                currentCollection++;

                yield return new WaitForEndOfFrame();
            }
    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value) {
            audioSource.loop = false;
            audioSource.clip = levelCopleted;
            audioSource.Play();

            StartCoroutine(ShowResults());
        }
    }
}
