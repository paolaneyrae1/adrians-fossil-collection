﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomizeController : MonoBehaviour {
    [SerializeField]
    private int maskSelected = -1;

    public MaskController maskController;
    public Button btnPlay;
    public GameObject messageTuto;
    public Image[] marks;

    void Start() {
        
        SetActiveMask(false);

        StartCoroutine(WaitToLoad());

    }

    private IEnumerator WaitToLoad() {
        yield return new WaitUntil(() => GameManager.loaded);


        bool anyCollection = true;
        SetActive(anyCollection);
        if (!anyCollection)
            Play();
    }


    private void SetActiveMask(bool value) {
        foreach (Image mark in marks)
            mark.enabled = value;
    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            GameManager.PlayBGOther();
        else
            GameManager.PlayBGMain();
    }

    public void SelectMask(int index) {
        SetActiveMask(false);

        maskSelected = (index != maskSelected) ? index : -1;

        if (maskSelected >= 0 && index < marks.Length)
            marks[index].enabled = true;

        maskController.SetActiveMaskByIndex(maskSelected);
        Player.SetMask(maskSelected);

        CancelInvoke();
   
        messageTuto.SetActive(index!=-1);

    }

    public void Play() {

        AnimationDirector.StartIntro();
        SetActive(false);
    }
}
