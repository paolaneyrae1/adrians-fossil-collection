﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour {
    private enum State {
        empty, loaded, moving
    }
    [SerializeField]
    private State state = State.empty;

    public bool isMoving {
        get => state == State.moving;
    }


    [SerializeField]
    private int currentLevel = 0;


    public float speed = 1f;
    public Transform[] levels;


    public Animator gateAnimator;
    public GateController gateController;

    public Lift other;

    public AudioClip clip;
    private AudioSource audioSource;

    public float minDistance = 1.4f;


    // Start is called before the first frame update
    void Start() {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.loop = false;
        audioSource.playOnAwake = false;
        audioSource.clip = clip;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.transform.parent = transform;
            LiftManager.Load(this);
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.transform.parent = null;
            LiftManager.Unload();
        }
    }

    public void PlayFx() {
        audioSource.Play();
    }

    public void StopFx() {
        audioSource.Stop();
    }

    public void PauseFx() {
        audioSource.Pause();
    }

    public void UnPauseFx() {
        audioSource.UnPause();
    }
}
