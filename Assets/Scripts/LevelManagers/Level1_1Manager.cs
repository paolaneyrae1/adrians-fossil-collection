﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1_1Manager : MonoBehaviour {

    static private Level1_1Manager instance;

    static public int coins {
        get => instance.coinsCollected;
        set => instance.coinsCollected = value;
    }

    static public int fossils {
        get => instance.fossilsCollected;
        set => instance.fossilsCollected = value;
    }

    static public int collections {
        get => instance.collectionsCollected;
        set => instance.collectionsCollected = value;
    }

    static public string currentMission {
        get => instance.misions[instance.currentMision].description;
    }

    static public string currentMissionKey {
        get => instance.lastMissionKey;
    }

    static public int extraTime {
        get => instance.timeExtra;
    }

    static public int extraTimeCollection {
        get => instance.timeExtraCollection;
    }

    [SerializeField]
    private int currentMision;
    private string lastMissionKey;

    public Mission[] misions;
    public GameObject[] missionsTriggers;

    public LevelResumeController levelResumeController;
    public LevelInfoController levelInfoController;

    public GameObject btnFinish;
    public GameObject btnHelp;

    private int coinsCollected;
    private int fossilsCollected;
    private int collectionsCollected;

    public int timeExtra = 3;
    public int timeExtraCollection = 20;


    void Awake() {    
        instance = this;
    }


    void Start() {
        levelResumeController.SetActive(false);

        btnFinish.SetActive(false);
        btnHelp.SetActive(false);

        foreach (Mission mision in misions)
            mision.SetActive(false);

        foreach (GameObject trigger in missionsTriggers)
            trigger.SetActive(false);

        currentMision = 0;
        ResetData();
    }

    public void ResetData() {
        coinsCollected = 0;
        fossilsCollected = 0;
        collectionsCollected = 0;
    }

    public void CloseMision() {
        levelInfoController.mision = misions[currentMision].description;

        misions[currentMision].Hide();
        currentMision++;
    }


    public void ShowMision() {
        misions[currentMision].Show();

        levelInfoController.mision = misions[currentMision].description;
        lastMissionKey = misions[currentMision].key;
    }
    public void FinishLevel() {
        levelResumeController.timeFormated = GameManager.timeFormated;
        levelResumeController.score = coinsCollected;
        levelResumeController.fossils = fossilsCollected;
        levelResumeController.collections = collectionsCollected;
        levelResumeController.SetActive(true);


        GameManager.StopTimer();
        GameManager.FinishGame();
        GameLoader.SetCoins(GameManager.currentScore);
        GameLoader.FinishCurrentLevel();
        GameLoader.SetFossils(Inventory.ExportFossils());
        GameLoader.SetMuseum(CollectionsManager.ExportMuseum());
        GameLoader.Save();
        
        Player.Lock();
        
    }

}
