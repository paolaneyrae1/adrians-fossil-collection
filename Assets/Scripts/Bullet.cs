﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 1f;
    public float torque = 100f;

    // Start is called before the first frame update
    void Start() {
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void Update() {
        transform.position += Vector3.right * speed * Time.deltaTime;

        transform.Rotate(Vector3.forward, torque * -Mathf.Sign(speed) * Time.deltaTime);
    }

    public void SetSprite(Sprite sprite) {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
    }

}
