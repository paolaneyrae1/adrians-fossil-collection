﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextUpdater : MonoBehaviour {

    public string group;
    public TextMeshProUGUI[] texts;

    void Start() {
        UpdateTexts();
    }

    public void UpdateTexts() {
        foreach (TextMeshProUGUI text in texts) {
            string[] cmds = text.text.Split('+');
            text.text = SharedState.LanguageDefs[group+cmds[0]] + (cmds.Length == 2 ? cmds[1].Replace("\"", "") : "");
        }

    }
}
