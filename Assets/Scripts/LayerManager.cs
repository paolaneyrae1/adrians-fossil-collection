﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerManager : MonoBehaviour {
    public List<Layer> layerTiles;
    public SpriteRenderer layerEndLeft;
    public SpriteRenderer layerEndRight;

    void Start() {
        layerTiles = new List<Layer>();
        layerTiles.AddRange(GetComponentsInChildren<Layer>());

        foreach (Layer layer in layerTiles) {
            layer.onDigEnd = RemoveTile;
            layer.onDigStart = (t, d) => {
                if (d == 1)
                    layerEndRight.enabled = false;
                else if (d == -1)
                    layerEndLeft.enabled = false;
            };
        }

    }

    private void RemoveTile(Layer tile, int direction) {
        layerTiles.Remove(tile);

        if (layerTiles.Count <= 0) {
            layerEndRight.enabled = false;
            layerEndLeft.enabled = false;
            return;
        }


        Vector3 pos;

        if (direction == 1) {
            pos = layerTiles[layerTiles.Count - 1].transform.position;
            pos.x += 6.4f * (layerEndRight.flipX ? 1f : -1f);
            layerEndRight.transform.position = pos;
            layerEndRight.enabled = true;
        }
        else if (direction == -1) {
            pos = layerTiles[0].transform.position;
            pos.x += 6.4f * (layerEndLeft.flipX ? 1f : -1f);
            layerEndLeft.transform.position = pos;
            layerEndLeft.enabled = true;
        }
    }
}
