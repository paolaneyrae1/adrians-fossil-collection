﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class ItemsDB : MonoBehaviour {

    static private ItemsDB instance;

    static public ItemData GetItemDataById(int id) {
        ItemData itemData = instance.items.Find(item => item.id == id);
        itemData.name = SharedState.LanguageDefs["fossilsf" + itemData.id + "name"];
        itemData.info1 = SharedState.LanguageDefs["fossilsf" + itemData.id + "descriptionLeft"];
        itemData.info2 = SharedState.LanguageDefs["fossilsf" + itemData.id + "descriptionRight"];

        return itemData;
    }

    static public ItemData[] GetItems(int quantity) {
        ItemData[] result = new ItemData[quantity];

        for (int i = 0; i < quantity; i++)
            result[i] = GetItemDataById(Random.Range(0, instance.items.Count));

        return result;
    }

    static public void LoadFossilsInfo(JSONNode data) {
        instance.LoadInfo(data);
    }

    public List<ItemData> items;

    void Start() {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void LoadInfo(JSONNode data) {
        items.ForEach(item => {
            item.name = data["f" + item.id]["name"];
            item.info1 = data["f" + item.id]["descriptionLeft"];
            item.info2 = data["f" + item.id]["descriptionRight"];
        });

    }
}
