﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDev : MonoBehaviour {
#if UNITY_EDITOR

    public Inventory inventory;
    public ItemData item;

    void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            inventory.AddItem(item);
    }
#endif
}
