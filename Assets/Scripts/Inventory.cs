﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inventory : MonoBehaviour {

    static private Inventory instance;

    static public int fossilsCont {
        get => instance.items.Count;
    }

    static public int fossilsTotal {
        get {
            int total = 0;

            instance.items.ForEach(i => total += i.stack);

            return total;
        }
    }

    static public void FossilDetail(ItemData item) {
        InspectorController.FillData(item);
        InspectorController.Show();
    }

    static public void FossilSelect(int index) {
        if (instance.currentSlotIndex >= 0)
            instance.GetSlotByIndex(instance.currentSlotIndex).Selected(false);

        instance.GetSlotByIndex(index).Selected(true);
        instance.currentSlotIndex = index;
    }

    static public void NextFossil() {
        int index = instance.currentSlotIndex + 1;

        if (index >= instance.slotsCount)
            index = 0;

        InspectorController.FillData(instance.GetSlotByIndex(index).item);
        FossilSelect(index);
    }

    static public void PrevFossil() {
        int index = instance.currentSlotIndex - 1;

        if (index < 0)
            index = instance.slotsCount - 1;

        InspectorController.FillData(instance.GetSlotByIndex(index).item);
        FossilSelect(index);
    }

    static public Sprite GetFossilSprite() {
        Sprite sprite = null;

        if (instance.items.Count == 0)
            return sprite;

        ItemData itemData = instance.items[0];

        sprite = itemData.sprite;
        itemData.stack--;

        if (itemData.stack > 0)
            instance.items[0] = itemData;
        else
            instance.items.RemoveAt(0);

        instance.UpdateSlots();

        return sprite;
    }

    static public List<Fossil> ExportFossils() {
        List<Fossil> result = new List<Fossil>();

        instance.items.ForEach(item => {
            result.Add(new Fossil() { item_id = item.id, stack = item.stack });
        });

        return result;
    }

    private int currentSlotIndex;

    private int slotsCount {
        get => content.childCount;
    }

    [SerializeField] private List<ItemData> items;
    [SerializeField] private int pages;
    [SerializeField] private int currentPage;


    public Slot slotTemplate;
    public RectTransform content;

    public int slotsPerPage = 15;

    public TextMeshProUGUI label;

    void Awake() {
        instance = this;

        items = new List<ItemData>();
        currentPage = 1;
        currentSlotIndex = -1;
        UpdateSlots();
    }

    private Slot GetSlotByIndex(int index) {
        return content.GetChild(index).GetComponent<Slot>();
    }

    private void ClearSlots() {
        int childCount = content.childCount;

        for (int i = 0; i < childCount; i++)
            Destroy(content.GetChild(i).gameObject);
    }

    private void UpdateSlots() {
        pages = Mathf.Max(1, Mathf.CeilToInt(items.Count / ((float)slotsPerPage)));

        ClearSlots();

        Slot slot;
        for (int i = (currentPage - 1) * slotsPerPage; i < (currentPage) * slotsPerPage; i++) {
            if (i >= items.Count)
                break;

            slot = Instantiate(slotTemplate);
            slot.sprite = items[i].sprite;
            slot.stack = items[i].stack;
            slot.item = items[i];
            slot.SetParent(content);
        }

        label.text = currentPage + " / " + pages;
    }

    public void AddItem(ItemData item) {
        int index = items.FindIndex(i => i.id == item.id);

        if (index >= 0) {
            ItemData itemData = items[index];
            itemData.stack += item.stack;
            items[index] = itemData;
        }
        else
            items.Add(item);

        UpdateSlots();
    }

    public void NextPage() {
        currentPage++;
        currentPage = Mathf.Min(currentPage, pages);

        UpdateSlots();
    }

    public void PrevPage() {
        currentPage--;
        currentPage = Mathf.Max(currentPage, 1);

        UpdateSlots();
    }


}
