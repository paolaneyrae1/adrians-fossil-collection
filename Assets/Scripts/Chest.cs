﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    Animator anim;
    private AudioSource audioSource;


    public Transform glow;
    public CollectionPart collectionPart;


    public delegate void OpenEnd();
    public event OpenEnd openEnd;

    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();
        anim = gameObject.GetComponent<Animator>();
    }

    public void OnOpenEnd() {
        openEnd?.Invoke();
    }

    public void PlayFX() {
        audioSource.Play();
    }
    public void OpenChest() 
    {
        anim.SetTrigger("Open");
    }
}
