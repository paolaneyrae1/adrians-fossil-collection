﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cure : MonoBehaviour {

    public AudioClip clip;

    public void Pick() {
        GameObject fx = new GameObject("FX");
        AudioSource audio = fx.AddComponent<AudioSource>();
        audio.clip = clip;
        audio.playOnAwake = false;
        audio.loop = false;
        audio.Play();

        Destroy(fx, clip.length);
        Destroy(gameObject);
    }
}
