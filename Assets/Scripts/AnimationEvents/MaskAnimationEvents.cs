﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskAnimationEvents : MonoBehaviour {

    public void EmptyEvent() { }

    public void OnAnimationEnd() {
        gameObject.SetActive(false);
    }
}
