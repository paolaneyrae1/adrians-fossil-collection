﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDirector : MonoBehaviour {

    static private AnimationDirector instance;
    public bool showCustomizePanel = true;

    static public void StartIntro() {
        instance.Init();
    }


    private enum State {
        idle, main, intro
    }
    private State state = State.idle;

    public Animator animator;
    public Animator cameraAnimator;
    public Animator[] pits;

    public PlayerController player;
    public GameObject lift;


    public Level1_1Manager levelManager;
    public InfoManager infoManager;

    public bool intro = true;


    void Awake() {
        instance = this;
    }

    void Start() {
        
        if (!showCustomizePanel) 
            StartIntro();
    }

    void Update() {

    }


    public void Init() {
        if (intro)
            animator.SetTrigger("main");
        else {
            Player.Lock();
            player.gameObject.SetActive(false);
            lift.SetActive(false);

            OnAnimationEnd();
        }
    }


    public void OnAnimationStart() {
        state = State.main;

        Player.Lock();
        player.gameObject.SetActive(false);
        lift.SetActive(false);

        //pit.SetActive(false);
        foreach (Animator pit in pits)
            pit.gameObject.SetActive(false);

        infoManager.ShowPanel(0);
    }

    public void OnAnimationEnd() {
        state = State.idle;

        if (!infoManager.isActive)
            PlayIntro();
    }

    public void OnIntroStart() {
        state = State.intro;

        PitAnimationStart();
        player.PlayIntro();
    }

    public void OnIntroEnd() {
        state = State.idle;

        levelManager.ShowMision();
    }

    public void PlayIntro() {
        if (state == State.idle)
            animator.SetTrigger("intro");
    }

    public void CameraAnimationStart() {
        cameraAnimator.SetBool("start", true);
    }

    public void CameraZoomIn() {
        if (cameraAnimator)
            cameraAnimator.SetTrigger("zoom_in");
    }

    public void PitAnimationStart() {
        lift.SetActive(true);

        foreach (Animator pit in pits) {
            pit.gameObject.SetActive(true);
            if (intro)
                pit.SetBool("start", true);
        }
    }
}
