﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationEvents : MonoBehaviour {

    [SerializeField]
    private SpriteRenderer[] sprites;

    private ParticleSystem particle;
    public Enemy enemy;

    void Start() {
        sprites = GetComponentsInChildren<SpriteRenderer>();
    }


    public void OnDigStart() {
        foreach (SpriteRenderer sprite in sprites)
            sprite.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;

        particle = Instantiate(enemy.particle, enemy.transform.position, enemy.particle.transform.rotation);
    }

    public void OnDigEnd() {
        foreach (SpriteRenderer sprite in sprites) {
            sprite.maskInteraction = SpriteMaskInteraction.None;
            //Debug.Log(sprite.name + " " + sprite.maskInteraction);
        }

        enemy.Idle();
        Enemy.Resgister(enemy);

        particle.Stop();
        Destroy(particle, 0.25f);
    }

    public void OnDeadEnd() {
        Enemy.Unregister(enemy);
        enemy.Die();
    }
    public void OnAttackStart() {
        enemy.Attack();
    }

    public void OnAttackEnd() {
        enemy.Idle();
    }

    public void OnHitEnd() {
        enemy.HitEnd();
    }


}
