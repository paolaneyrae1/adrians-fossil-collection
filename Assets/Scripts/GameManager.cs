﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour {

    static private GameManager instance;

    static public void ScoreAdd(int value) {
        Level1_1Manager.coins++;
        instance.AddScore(value);
    }

    static public void ScoreDecrease(int value) {
        instance.AddScore(-value);
    }

    static public void AddItem(ItemData item) {
        Level1_1Manager.fossils++;
        instance.inventoryController.AddItem(item);
    }

    static public void AddPurchasedItem(ItemData item) {
        instance.inventoryController.AddItem(item);
    }

    static public void AddCollection(string id) {
        Level1_1Manager.collections++;

        int index = (int.Parse(id.Substring(1))) - 1;

        switch (id[0])
        {
            case '0':
                if (CollectionsManager.CheckCollectionByIndex(0))
                    break;

                CollectionsManager.SetActiveTrexPart(index, true);

                if (CollectionsManager.CheckCollectionByIndex(0))
                {
                    BadgeController.Show(0);
                    CollectionsManager.EnableMedal(0);
                }

                break;

            case '1':
                if (CollectionsManager.CheckCollectionByIndex(1))
                    break;

                CollectionsManager.SetActiveTriceratopsPart(index, true);

                if (CollectionsManager.CheckCollectionByIndex(1))
                {
                    BadgeController.Show(1);
                    CollectionsManager.EnableMedal(1);
                }

                break;

            case '2':
                if (CollectionsManager.CheckCollectionByIndex(2))
                    break;

                CollectionsManager.SetActiveVelociraptorPart(index, true);

                if (CollectionsManager.CheckCollectionByIndex(2)) 
                { 
                    BadgeController.Show(2);
                    CollectionsManager.EnableMedal(2);
                }

                break;
                
        }


    }

    static public void UnlockCollection(int index) {
        switch (index) {
            case 0:
                for (int i = 0; i < 10; i++)
                    CollectionsManager.SetActiveTrexPart(i, true);

                break;

            case 1:
                for (int i = 0; i < 8; i++)
                    CollectionsManager.SetActiveTriceratopsPart(i, true);

                break;

            case 2:
                for (int i = 0; i < 8; i++)
                    CollectionsManager.SetActiveVelociraptorPart(i, true);

                break;
        }
    }

    static public void StopTimer() {
        instance.timer.Stop();
    }

    static public void PauseTimer() {
        instance.timer.Pause();
    }

    static public void PlayTimer() {
        instance.timer.Play();
    }

    static public void AddTime(int time, Vector3 position) {
        instance.timer.AddTime(time);
        TextFx clone = Instantiate(instance.textFxPrefab, position, Quaternion.identity);
        clone.value = time;
    }


    static public void PlayGame() {
        instance.state = State.game;
    }

    static public void PauseGame() {
        instance.state = State.pause;
    }

    static public void FinishGame() {
        instance.state = State.finish;
    }

    static public void TimesUp() {
        instance.state = State.finish;

        instance.timesUpController.SetActive(true);
    }

    static public void GameOver() {
        instance.state = State.finish;

        instance.gameOverPanel.SetActive(true);
        StopTimer();
    }

    static public void ShowDatingPanel(int index) {
        
        instance.datingPanel[index].Show();
    }

    static public void PlayBGMain() {
        instance.audioSource.Stop();
        instance.audioSource.clip = instance.main;
        instance.audioSource.Play();
    }

    static public void PlayBGOther() {
        instance.audioSource.Stop();
        instance.audioSource.clip = instance.other;
        instance.audioSource.loop = true;
        instance.audioSource.Play();
    }

    static public int time {
        get => instance.timer.timeInSeconds;
    }

    static public string timeFormated {
        get => instance.timer.timeFormated;
    }

    static public int currentScore {
        get => instance.score;
    }

    static public State currentState {
        get => instance.state;
    }

    static public bool loaded {
        get => instance._loaded;
    }

    private bool _loaded = false;

    public enum State {
        intro, game, pause, finish
    }
    public State state = State.intro;

    public PlayerData playerData;

    [Header("Score")]
    public int score;
    public TextMeshProUGUI txtScore;

    [Header("Timer")]
    public Timer timer;

    [Header("Inventory")]
    public InventoryController inventoryController;
    public StoreController storeController;

    [Header("Collections")]
    public CollectionsController collectionsController;

    [Header("Pause Menu")]
    public PauseMenuController pauseMenuController;

    [Header("Level info")]
    public LevelInfoController levelInfoController;

    [Header("Time¡s up")]
    public TimesUpController timesUpController;

    [Header("Game Over")]
    public GameOverPanel gameOverPanel;


    [Header("Dating")]
    public DatingPanel[] datingPanel;

    [Header("Audio BG")]
    private AudioSource audioSource;
    public AudioClip main;
    public AudioClip other;

    [Header("FX")]
    public TextFx textFxPrefab;

    void Awake() {
        instance = this;

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.loop = false;
    }

    void Start() {

        Time.timeScale = 1;
        UpdateScore();

        Load();
    }

    void Update() {
        if (state == State.intro || state == State.finish)
            return;

        if (Input.GetKeyDown(KeyCode.I)) {
            if (!inventoryController.isOpen) {
                CloseCollections();
                OpenInventory();
            }
            else {
                CloseCollections();
                CloseInventory();
                CloseStore();
            }
        }

        if (Input.GetKeyDown(KeyCode.C)) {
            if (!collectionsController.isOpen) {
                CloseInventory();
                CloseStore();
                OpenCollections();
            }
            else {
                CloseCollections();
                CloseInventory();
                CloseStore();
            }
        }

    }
    public void GoToMainMenu() {
        SceneManager.LoadScene(0);
    }

    public void GoToLevel(int index) {
        SceneManager.LoadScene(index);
    }

    public void OpenInventory() {
        state = State.pause;

        timer.Pause();
        inventoryController.SetActive(true);
    }

    public void CloseInventory() {
        state = State.game;

        timer.Play();
        inventoryController.SetActive(false);
    }
    public void OpenStore() {
        storeController.SetActive(true);
    }

    public void CloseStore() {
        storeController.SetActive(false);
    }
    public void OpenCollections() {
        state = State.pause;

        timer.Pause();
        collectionsController.OnpenClose(true);
    }
    public void CloseCollections() {
        state = State.game;

        timer.Pause();
        collectionsController.OnpenClose(false);
    }

    public void AddScore(int value) {
        score += value;

        UpdateScore();
    }

    public void UpdateScore() {
        txtScore.text = score.ToString("000");
    }

    public void Play() {
        state = State.game;
    }

    public void Pause() {
        Debug.Log("GameManager -> Pause");
        state = State.pause;


        Time.timeScale = 0;

        pauseMenuController.SetActive(true);
        pauseMenuController.SetScore(score);
        pauseMenuController.SetTime(timer.timeFormated);
    }

    public void Continue() {
        Debug.Log("GameManager -> Continue");
        state = State.game;


        Time.timeScale = 1;

        pauseMenuController.SetActive(false);
    }

    public void Finish() {
        state = State.finish;
    }

    public void ShowLevelInfo() {
        state = State.pause;

        Time.timeScale = 0;

        levelInfoController.SetActive(true);
    }

    public void HideLevelInfo() {
        state = State.game;

        Time.timeScale = 1;

        levelInfoController.SetActive(false);
    }


    public void Save() {
        GameLoader.Save();
    }

    public void Load() {
        GameLoader.Load(state => {
            GameLoader.SetCurrentLevelIndex(SceneManager.GetActiveScene().buildIndex);

            
            AddScore(state.coins);

            state.fossils.ForEach(fossil => {
                ItemData item = ItemsDB.GetItemDataById(fossil.item_id);
                item.stack = fossil.stack;
                GameManager.AddItem(item);
            });


            for (int i = 0; i < state.museum.trex.Length; i++)
                CollectionsManager.SetActiveTrexPart(i, state.museum.trex[i]);

            for (int i = 0; i < state.museum.triceratops.Length; i++)
                CollectionsManager.SetActiveTriceratopsPart(i, state.museum.triceratops[i]);

            for (int i = 0; i < state.museum.velociraptor.Length; i++)
                CollectionsManager.SetActiveVelociraptorPart(i, state.museum.velociraptor[i]);

            _loaded = true;
        });
    }

}
