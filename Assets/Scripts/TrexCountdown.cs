﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrexCountdown : MonoBehaviour {

    static private TrexCountdown instance;


    static public void Roar() {
        instance.StartCoroutine(instance.Cooldown());
    }

    static public float fillAmount {
        get => instance.fill.fillAmount;
        set => instance.fill.fillAmount = value;
    }

    static public float stunTime { get => instance.stun; }

    private AudioSource audioSource;

    public Image fill;
    public float cooldown = 5f;
    public float stun = 3f;


    void Awake() {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    private IEnumerator Cooldown() {
        float t = 0;

        if (audioSource.clip)
            audioSource.Play();

        while (t < 1f) {
            fillAmount = t;

            t += Time.deltaTime / cooldown;

            yield return new WaitForEndOfFrame();
        }

        fillAmount = 1f;
    }
}
