﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieManager : MonoBehaviour {

    public EnemySpawner[] spawners;
    public List<Enemy> enemies;

    void Start() {
        enemies = new List<Enemy>();
        EnemySpawner.OnSpawn += (enemy) => {
            enemies.Add(enemy);
        };

        foreach (EnemySpawner spawner in spawners)
            spawner.gameObject.SetActive(false);
    }

    private void ShowSpawners() {
        foreach (EnemySpawner spawner in spawners)
            spawner.gameObject.SetActive(true);
    }

    public void ActiveSpawners() {
        Invoke("ShowSpawners", 0.01f);
    }

    public void IdleAllEnemies() {
        Enemy.isLocked = true;
    }
}
