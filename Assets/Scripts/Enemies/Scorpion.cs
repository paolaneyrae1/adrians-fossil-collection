﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion : Enemy {

    public override void Hit(int damage) {
        base.Hit(damage);
    }

    public override void Attack() {
        base.Attack();
        this.direction = -Mathf.Sign(transform.position.x - Player.position.x);
    }
}
