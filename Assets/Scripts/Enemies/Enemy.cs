﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    static public bool isLocked = false;

    static private List<Enemy> enemies = new List<Enemy>();


    static public void Resgister(Enemy enemy) {
        if (enemy == null) enemies = new List<Enemy>();


        if (enemies.Find(e => e == enemy) == null)
            enemies.Add(enemy);
    }

    static public void Unregister(Enemy enemy) {
        if (enemy == null) return;

        enemies.Remove(enemy);
    }

    static public void ForEach(System.Action<Enemy> action) {
        Debug.Log("Enemy -> ForEach " + enemies.Count);
        enemies.ForEach(action);
    }




    private CircleCollider2D collider2D;
    private AudioSource audioSource;

    public int hp;
    public float range = 2f;
    public float distance = 1f;
    public LayerMask layerMask;
    public float speed = 1f;
    public int damage = 1;

    public Animator animator;
    public GameObject loot;
    public Vector3 offset;

    public ParticleSystem particle;


    public enum State {
        dig, idle, walk, hit, attack, dead, stunned
    }
    public State state = State.dig;

    public float direction {
        get => transform.localScale.x;
        set => transform.localScale = new Vector3(value, 1f, 1f);
    }

    [System.Serializable]
    public struct PlayerSoundFx {

        public AudioClip death;
    }

    [Header("FX")]
    public PlayerSoundFx fxs;
    protected void Start() {
        collider2D = GetComponent<CircleCollider2D>();
        audioSource = GetComponent<AudioSource>();

        animator = transform.GetChild(0).GetComponent<Animator>();
    }

    protected void Update() {

        if (isLocked || GameManager.currentState == GameManager.State.pause || GameManager.currentState == GameManager.State.finish) {
            animator.speed = 0;
            return;
        }
        else {
            animator.speed = 1f;
        }

        if (state != State.walk && state != State.idle)
            return;


        Vector2 origin = (Vector2)transform.position + (collider2D.offset * transform.up.y);
        Vector2 direction = ((new Vector2(Mathf.Sign(this.direction) * distance, -1f)).normalized * transform.up.y);
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, range, layerMask);


        if (hit.collider != null) {
            Debug.DrawLine(origin, hit.point, Color.green);

            state = State.walk;

            if (hit.collider.CompareTag("Layer"))
                this.direction *= -1f;
        }
        else {
            Debug.DrawLine(origin, origin + (direction * range));
            state = State.idle;

            this.direction *= -1f;
        }


        if (state == State.walk)
            transform.position += transform.right * Mathf.Sign(this.direction) * speed * Time.deltaTime;

        animator.SetBool("walk", state == State.walk);
    }

    protected void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log("OnTriggerEnter2D: " + collision.name);
        if (collision.name == "Manito_3") {
            Hit(collision.GetComponentInParent<Player>().damage);
        }
        else if (collision.name.StartsWith("bullet")) {
            Destroy(collision.gameObject);
            Hit(1);
        }

        if (hp <= 0) {
            animator.SetTrigger("dead");
            collider2D.enabled = false;

            audioSource.clip = fxs.death;
            audioSource.loop = false;
            audioSource.Play();
        }
    }

    protected void OnCollisionStay2D(Collision2D collision) {
        if (state == State.dig)
            return;

        if (collision.gameObject.CompareTag("Player"))
            Attack();
    }

    protected void OnDestroy() {
        Enemy.Unregister(this);
    }


    public void Idle() {
        state = State.idle;
        animator.SetBool("attack", false);
        animator.SetBool("stun", false);
    }

    public void Die() {
        Instantiate(loot, transform.position + offset, Quaternion.identity);
        Destroy(gameObject);
    }

    public virtual void Attack() {
        state = State.attack;
        animator.SetBool("attack", true);

        this.direction = -Mathf.Sign(transform.position.x - Player.position.x) * Mathf.Sign(transform.up.y);
    }

    public virtual void Hit(int damage) {
        if (state == State.dig)
            return;

        state = State.hit;

        hp -= damage;
        animator.SetTrigger("hit");

        this.direction = -Mathf.Sign(transform.position.x - Player.position.x) * transform.up.y;
    }

    public void HitEnd() {
        state = State.idle;
    }

    public void Stun() {
        state = State.stunned;
        animator.SetBool("stun", true);
    }

}
