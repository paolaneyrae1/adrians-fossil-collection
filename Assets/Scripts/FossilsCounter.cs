﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FossilsCounter : MonoBehaviour {

    public TextMeshProUGUI txtCounter;

    void Update() {
        txtCounter.text = Inventory.fossilsTotal.ToString();
    }
}
