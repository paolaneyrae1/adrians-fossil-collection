﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuController : MonoBehaviour {

    public TextMeshProUGUI[] texts;


    void Start() {
        UpdateTexts();
    }

    public void LoadLevelByIndex(int index) {
        SceneManager.LoadScene(index);
    }

    public void UpdateTexts() {
        foreach (TextMeshProUGUI text in texts) {
            string[] cmds = text.text.Split('+');
            text.text = SharedState.LanguageDefs["mainMenu"+cmds[0]] + (cmds.Length == 2 ? cmds[1].Replace("\"", "") : "");
        }

    }
}
