﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class CollectionsController : MonoBehaviour {

    static private CollectionsController instance;

    static public void UpdateDescription(string description) {
        instance.txtDescription.text = description;
    }

    static public void HideDescription() {
        instance.txtDescription.transform.parent.gameObject.SetActive(false);
    }

    static public void ShowDescription() {
        instance.txtDescription.transform.parent.gameObject.SetActive(true);
    }


    public TextMeshProUGUI txtDescription;
    public TextMeshProUGUI[] texts;

    public UnityEvent onOpen;

    public bool isOpen {
        get { return gameObject.activeSelf; }
    }


    void Awake() {
        instance = this;
    }

    // Start is called before the first frame update
    void Start() {

        SetActive(false);
        UpdateTexts();
    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (isOpen)
            onOpen?.Invoke();
    }

    public void OnpenClose(bool value) {
        SetActive(value);

        if (value)
            GameManager.PlayBGOther();
        else
            GameManager.PlayBGMain();
    }

    public void UpdateTexts() {
        foreach (TextMeshProUGUI text in texts) {
            string[] cmds = text.text.Split('+');
            text.text = SharedState.LanguageDefs["collectionsPanel"+cmds[0]] + (cmds.Length == 2 ? cmds[1].Replace("\"", "") : "");
        }

        UpdateDescription(SharedState.LanguageDefs["collectionstrex"]);

    }
}
