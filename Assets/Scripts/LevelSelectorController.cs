﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectorController : MonoBehaviour {

    public Button[] buttons;

    void Start() {
#if !UNITY_EDITOR
        float maxLevel = GameLoader.GetCurrentLevelIndex();
        for(int i = 0; i < buttons.Length; i++)
            buttons[i].interactable = i < maxLevel;
#endif
    }

    public void GoToLevel(int index) {
        SceneManager.LoadScene(index);
    }
}
