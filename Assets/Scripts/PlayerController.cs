﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Coroutine intro;


    public Player player;

    [Header("Intro")]
    public Vector3 startPosition;
    public Vector3 endPosition;

    private void IntroStart() {
        transform.position = startPosition;
        intro = StartCoroutine(Intro());
    }

    private IEnumerator Intro() {
        player.AutoMove();

        float distance = Vector3.Distance(transform.position, endPosition);
        while (distance >= 1f) {
            yield return new WaitForEndOfFrame();
            distance = Vector3.Distance(transform.position, endPosition);
        }

        IntroEnd();
    }
    private void IntroEnd() {
        transform.position = endPosition;
        player.Stop();
        player.Lock(true);
        intro = null;
    }


    public void PlayIntro() {
        gameObject.SetActive(true);
        IntroStart();
    }

}
