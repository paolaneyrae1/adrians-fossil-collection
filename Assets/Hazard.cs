﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    Animator animator;
    public int damage = 1;
    bool ableHit=true;

    private void Start()
    {
        animator = GetComponent<Animator>();

    }

    private void OnCollisionEnter2D( Collision2D col)
    {
        if (col.gameObject.CompareTag("Player")&& ableHit) 
        {
            col.gameObject.GetComponent<Player>().Hit(damage);
            ableHit = false;
            StartCoroutine(TimeAbleHit());
        }
    }
    IEnumerator TimeAbleHit() 
    {
        yield return new WaitForSeconds(1.5f);
        ableHit = true;

    }
}
